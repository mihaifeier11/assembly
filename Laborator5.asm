;Se da un numar natural negativ a (a: dword). Sa se afiseze valoarea lui in baza 10 si in baza 16, in urmatorul format: "a = <base_10> (baza 10), a = <base_16> (baza 16)"
bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit               ; tell nasm that exit exists even if we won't be defining it
extern scanf
import scanf msvcrt.dll
extern printf
import printf msvcrt.dll
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    n dd  0       ; definim variabila n
	formatscan  db "%d", 0  ; definim formatul
    formatprint10  db "a = <base 10> < %d >, ", 0 
    formatprint16  db "a = <base 16> < %x >", 0 

; our code starts here
segment code use32 class=code
    start:
        push dword n       ; punem parametrii pe stiva de la dreapta la stanga
		push dword formatscan
		call [scanf]       ; apelam functia scanf pentru citire
		add esp, 4 * 2     ; eliberam parametrii de pe stiva
    
        push dword [n]  ; punem pe stiva valoarea lui n
		push dword formatprint10 
		call [printf]       ; apelam functia printf
		add esp, 4 * 2     ; eliberam parametrii de pe stiva
        
        push dword [n]  ; punem pe stiva valoarea lui n
		push dword formatprint16
		call [printf]       ; apelam functia printf
		add esp, 4 * 2     ; eliberam parametrii de pe stiva
        
        
    
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
