bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit, scanf, printf               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions
import scanf msvcrt.dll
import printf msvcrt.dll
; our data is declared here (the variables needed by our program)
segment data use32 class=data
    format_16       db "%x", 0
    format_10       db "%d", 0
    format_print    db "%d ", 0
    numar_cifre     db 0
    sir             times 10 dd 0
    value           db 0
    
; our code starts here
segment code use32 class=code
    start:
        push dword numar_cifre
        push dword format_10
        call [scanf]
        add ESP, 4*2
        
        mov ECX, [numar_cifre]
        mov ESI, 0
        citire:
            pushad
            
            push dword value
            push dword format_16
            call [scanf]
            add ESP, 4*2
            
            mov eax, [value]
            mov [sir+esi], eax
            
            inc ESI
            
            popad
        loop citire
        
        
        mov ESI, 0
        mov ECX, [numar_cifre]
        afisare:
            
            pushad
            
            mov eax, [sir+esi]
            
            push eax
            push dword format_10
            call [printf]
            add ESP, 4*2
            
            inc ESI
            
            popad
        loop afisare
        
        
        
    
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
