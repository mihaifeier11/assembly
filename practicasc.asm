bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit, fopen, fread, scanf, printf, fprintf, fclose
import exit msvcrt.dll    
import fopen msvcrt.dll
import fread msvcrt.dll 
import scanf msvcrt.dll
import printf msvcrt.dll
import fprintf msvcrt.dll
import fclose msvcrt.dll

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    nume_fisier db "output.txt", 0
    descriptor dd -1
    mod_acces db "a", 0
    format db "%d", 0
    numar db "text", 0

; our code starts here
segment code use32 class=code
    start:    
        push dword mod_acces
        push dword nume_fisier
        call [fopen]
        add esp, 4*2
        mov [descriptor], eax
        
        cmp eax, 0
        je final
        
        repeta:
            push dword numar
            push dword format
            call [scanf]
            add esp, 4*2
            
            cmp dword [numar], 0
            je final
            
            
            
        loop repeta
        
        
    
        final:
        push dword [numar]
        push dword [descriptor]
        call [fprintf]
        add esp, 4*2
        
        push dword [descriptor]
        call [fclose]
        add esp, 4
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
