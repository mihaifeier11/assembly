;(a-2)/(b+c)+a*c+e-x
;a,b-byte; c-word; e-doubleword; x-qword
;fara semn

bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    a db 3
    b db 3
    c dw 2
    e dd 1
    x dq 5

; our code starts here
segment code use32 class=code
    start:
        mov AL, [a] ;AL = a
        sub AL, 2 ;AL = a - 2
        mov BL, AL; BL = AL = a- 2
        
        mov AL, [b] ;Al = b
        cbw
        add AX, [c] ;AX = b + c
        
        mov CX, AX; CX = b + c
        
        mov AL, BL ;AL = a - 2
        cbw
        cwd ;AX -> DX:AX
        
        idiv CX ;AX = DX:AX/CX = (a - 2)/(b + c)
        
        mov BX, AX ;BX = AX
        
        mov AL, [a] ;AL = a
        cbw ;AX = AL
        imul WORD [c] ;DX:AX = a*c
        
        push DX
        push AX
        pop EDX; EDX = a*c
        
        mov AX, BX; AX = (a-2)/(b + c)
        cwde ;
        
        add EAX, EDX ;EAX = (a-2)/(b + c) + a*c
        add EAX, [e]; EAX = (a-2)/(b + c) + a*c + e
        
        mov EDX, 0
        
        sub EAX, DWORD [x]
        sbb EDX, DWORD [x + 4]
        
    
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
