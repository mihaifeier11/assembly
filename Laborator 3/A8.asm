;a - byte, b - word, c - double word, d - qword
;(a+b-d)+(a-b-d)
bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    a db 3
    b dw 2
    c dd 4
    d dq 1

; our code starts here
segment code use32 class=code
    start:
        mov AL, [a] ;AL = a
        CBW ;AX = AL
        add AX, [b] ;AX = AX + b
        CWDE ;EAX = DX:AX   
        CDQ ;EDX:EAX = EAX
        sub EAX, [d] ;EAX = EAX - d [low]
        sbb EDX, [d+4] ;EDX = EDX - d [high] - CF
        mov ECX, EDX ;ECX = EDX
        mov EBX, EAX ;EBX = EAX
        
        mov AL, [a] ;AL = a
        CBW ;AX = AL
        sub AX, [b] ;AX = AX - b
        CWDE ;EAX = AX
        CDQ ;EDX:EAX = EAX
        sub EAX, [d] ;EAX = EAX - d [low]
        sbb EDX, [d+4] ;EDX = EDX - d [high] - CF
        add EAX, EBX ;EAX = EAX + EBX
        adc EDX, ECX ;EDX = EDX + ECX

    
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
