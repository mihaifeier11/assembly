;a - byte, b - word, c - double word, d - qword
;(b+c+d)-(a+a)
bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    a db 3
    b dw 2
    c dd 4
    d dq 1

; our code starts here
segment code use32 class=code
    start:
        mov AX, [b] ;AX = b
        CWDE ;EAX = AX
        add EAX, [c] ; EAX = EAX + c
        CDQ ;EDX:EAX = EAX
        add EAX, [d] ;EAX = EAX + d [low]
        adc EDX, [d+4] ;EDX = EDX + d [high] + CF
        mov EBX, EAX ;EBX = EAX
        mov ECX, EDX ;ECX = EDX
        mov AL, [a] ;AL = a
        CBW ;AX = AL
        add AX, AX ; AX = AX + AX
        CWDE ;EAX = AX
        CDQ ;EDX:EAX = EAX
        sub EBX, EAX ;EBX = EBX - EAX
        sbb ECX, EDX ;ECX = ECX - EDX
        mov EAX, EBX ;EAX = EBX
        mov EDX, ECX ;EDX = ECX
    
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
