;(a-2)/(b+c)+a*c+e-x
;a,b-byte; c-word; e-doubleword; x-qword
bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    a db 3
    b db 3
    c dw 2
    e dd 1
    x dq 5

; our code starts here
segment code use32 class=code
    start:
        mov AL, [a] ;AL = a
        sub AL, 2 ;AL = a-2
        mov ah,0
        
        mov BL, [b] ; BL = b
        mov BH, 0 ;BH = 0
        
        add BX, [c] ;BX = b+c
        
        mov DX, 0 ;DX = 0
        
        div BX ;AX = DX:AX/ BX = (a-2)/(b+c)
        
        mov BX, AX ;BX = AX
        
        mov AL, [a] ;AL = a
        mov AH, 0 ;AH = 0
        
        mul word [c] ;DX:AX = AX * c = a * c
        
        push DX
        push AX
        pop ECX ;ECX = DX:AX = a*c
        
        mov AX, BX ;AX = BX = (a-2)/(b+c)
        mov DX, 0 ;DX = 0
        
        push dx
        push ax
        pop eax
        
        add EAX, EcX ; EAX = EAX + EcX = (a-2)/(b+c) + a*c
        
        add EAX, [e] ;EAX = EAX + e = (a-2)/(b+c) + a*c + e
        
        mov EDX, 0
        
        sub EAX, [x] ;EAX = EAX - [x] = (a-2)/(b+c) + a*c - [x]
        sbb EDX, [x+4] ;EDX = EDX - [x+4]
        
        ;rezultatul se afla in EDX:EAX
        
        
        
    
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
