bits 32 

global start        

; declare external functions needed by our program
extern exit, fopen, fread, fclose, printf, scanf
import exit msvcrt.dll  
import fopen msvcrt.dll  
import fread msvcrt.dll
import fclose msvcrt.dll
import printf msvcrt.dll
import scanf msvcrt.dll

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    nume_fisier times 100 db 0  ; numele fisierului care va fi creat
    mod_acces db "r", 0          ; modul de deschidere a fisierului - 
                                 ; r - pentru scriere. fisierul trebuie sa existe 
    len equ 100                  ; numarul maxim de elemente citite din fisier.                            
    text times (len+1) db 0      ; sirul in care se va citi textul din fisier (dimensiune len+1 explicata mai sus)
    descriptor_fis dd -1         ; variabila in care vom salva descriptorul fisierului - necesar pentru a putea face referire la fisier
    format db "%s", 0  ; formatul - utilizat pentru afisarea textului citit din fisier
                                 ; %s reprezinta un sir de caractere
    format_c db "%c", 0
    format_caracter db " %c", 0
    caracter db 0
    format_nr db "%d", 0
    numar_repetitii db 0

; our code starts here
segment code use32 class=code
    start:
        push nume_fisier
        push format
        call [scanf]
        add esp, 4*2
        
        push numar_repetitii
        push format_nr
        call [scanf]
        add esp, 4*2
        
        push caracter
        push format_caracter
        call [scanf]
        add esp, 4*2
    
        push dword mod_acces     
        push dword nume_fisier
        call [fopen]
        add esp, 4*2               

        mov [descriptor_fis], eax  
        
        cmp eax, 0
        je final
        

        push dword [descriptor_fis]
        push dword len
        push dword 1
        push dword text        
        call [fread]
        add esp, 4*4                

        
        mov ECX, EAX
        mov ESI, -1
        
        repeta:
            inc ESI
            push ECX
            push ESI
            
            mov EAX, 0
            mov AL, [text+ESI]
            
            push dword EAX
            push dword format_c
            call [printf]
            add esp, 4*2
            
            mov BL, [caracter]
            cmp [text+ESI], BL
            jne sfarsit
            mov ECX, [numar_repetitii]
            sub ECX, 1
            scrie:
                push ecx
                mov EAX, 0
                mov AL, [text+ESI]
                push dword EAX
                push dword format_c
                call [printf]
                add esp, 4*2
                pop ecx
            loop scrie
            sfarsit:
            
            pop ESI
            
            pop ECX
        loop repeta
        
        
        push dword [descriptor_fis]
        call [fclose]
        add esp, 4
        
      final:
        
        ; exit(0)
        push    dword 0      
        call    [exit]       