bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit, printf               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
import printf msvcrt.dll                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    
    sir DB 2, 4, 2, 5, 2, 5, 4, 4 
    len equ ($-sir)
    rez resw len
    format db "%x ", 0
    lenRez db 0
    zer db '0', 0

; our code starts here
segment code use32 class=code
    start:
        ; ...
        mov ESI, sir            ; We put in ESI the adress of the first element in sir
        mov ECX, len            ; We put in ECX th length
        cld                     ; We clear the DF, so we know for sure that the direction is the one we want
        jecxz final
        bucla1:
            lodsb                   ;in AL we have the current element
            push esi                ;we put ESI on the stack in order to memorize it so we can also use it for the second string
            push ecx                ;we put ECX on the stack in order to memorize it so we can also use it for the second string
            mov esi, rez            ;we put the offset in ESI so we can use LODSW
            mov ecx, [lenRez]       ;we put the length of the second string in ECX
            mov bl, al              ;we put AL in BL so that we can use AL for the second string's LODSW
            mov edi, rez            ;we put the offset of rez in edi so we can put the elements back in memory
            mov dl, 0               ;we use it as a flag
            jecxz skip              ;if the length of the second string is 0, then we jump to the "skip" label
            bucla2:
                lodsw               ;in AX we have the current element from rez
                cmp al, bl          ;we compare the element from rez with the one from sir
                jne over            ;if they're not equal, then change nothing in rez
                add ah, 1           ;if they're equal we add one to the number of appearances
                mov dl, 1           ;we found the current element of sir in rez so we set the flag to 1, so we know to update th word from rez
                over:
                    stosw           ;we put the number back in rez
            loop bucla2
            cmp dl, 1               ;if the flag is one we don't need a new element in rez
            je noNew
            skip:                   ;we create a new word because we have found a new number in the string that hasn't been found before in the 2nd str
                mov al, bl          ;we move BL to AL in order to create AX
                mov ah, 1           ;it's the first time we have found the number, so AH is 1
                mov [edi], ax       ;we put AX in rez
                add [lenRez], byte 1;the length of rez increases with 1
            noNew:
                pop ecx             ;recovers ECX from the stack
                pop esi             ;recovers ESI from the stack
        loop bucla1
        
        ; Here we print the numbers
        
        mov ecx, [lenRez]
        mov esi, rez
        jecxz final
        bucla3:
            mov eax, 0
            mov ax, [esi]
            add esi, 2
            
            push ecx
            push eax
            push zer
            call [printf]
            add esp, 4
            pop eax
            pop ecx
            
            push ecx
            push dword eax
            push format
            call [printf]
            add esp, 4 * 2
            pop ecx
        loop bucla3
        final:
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
