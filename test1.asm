bits 32 

global start        

; declare external functions needed by our program
extern exit, fopen, fread, fclose, printf, fprintf, scanf
import exit msvcrt.dll  
import fopen msvcrt.dll  
import fread msvcrt.dll
import fprintf msvcrt.dll
import fclose msvcrt.dll
import printf msvcrt.dll
import scanf msvcrt.dll
; our data is declared here (the variables needed by our program)
segment data use32 class=data
    fisier_citire times 100 db 0
    fisier_afisare db "output.txt", 0
    mod_citire db "r", 0         
                                
    mod_afisare db "a", 0
    len equ 100                  
    text times (len+1) db 0      
    descriptor_citire dd -1       
    descriptor_afisare dd -1         
    format db "%c", 0  
    format_citire db " %c", 0
    format_string db "%s", 0        
    format_nr db "%d", 0
    sir times 200 db 0
    caracter db 0
    numar_repetitii db 0
    
; our code starts here
segment code use32 class=code
    start:
        push fisier_citire
        push format_string
        call [scanf]
        add esp, 4*2
        
        push numar_repetitii
        push format_nr
        call [scanf]
        add esp, 4*2
        
        push caracter
        push format_citire
        call [scanf]
        add esp, 4*2
    
        push dword mod_citire     
        push dword fisier_citire
        call [fopen]
        add esp, 4*2               

        mov [descriptor_citire], eax   
        
        cmp eax, 0
        je final
        
        push dword mod_afisare    
        push dword fisier_afisare
        call [fopen]
        add esp, 4*2              

        mov [descriptor_afisare], eax 
        
        cmp eax, 0
        je final
        
        push dword [descriptor_citire]
        push dword len
        push dword 1
        push dword text        
        call [fread]
        add esp, 4*4               
        
        mov ecx, eax
        mov esi, -1
        mov edi, -1
        repeta:
            inc edi
            inc esi
            push ecx
            push esi
            
            mov EAX, 0
            mov AL, [text+esi]
            mov [sir+edi], AL
            
            cmp AL, [caracter]
            jne sfarsit
            mov ecx, [numar_repetitii]
            dec ecx
            bucla:
                push ecx
                inc edi
                mov EAX, 0
                mov AL, [text+esi]
                mov [sir+edi], AL
                pop ecx
            loop bucla

            sfarsit:
            pop esi
            pop ecx
        loop repeta
        
        push dword sir
        push dword [descriptor_afisare]
        call [fprintf]
        add esp, 4*2
       
        push dword [descriptor_citire]
        call [fclose]
        add esp, 4
        
        push dword [descriptor_afisare]
        call [fclose]
        add esp, 4
      final:
        
        ; exit(0)
        push    dword 0      
        call    [exit]       