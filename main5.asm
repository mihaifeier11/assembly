bits 32
global start

import printf msvcrt.dll
import exit msvcrt.dll
import scanf msvcrt.dll
extern printf, scanf, exit

extern calcul

segment data use32
    a dd 0
    b dd 0
    c dd 0
    format db "%d", 0
    message1 db "a = ", 0
    message2 db "b = ", 0
    message3 db "c = ", 0
    printformat db "a + b - c: %d", 0

segment code use32 public code
start:
    push dword message1 ; push the parameter for message1 onto the stack
    call [printf]       ; call printf to print message1
    add ESP, 4*1        ; add 4 bytes to the stack to "jump" the parameters
        
    push dword a        ; push the parameter for a onto the stack
    push dword format   ; push the parameter for format onto the stack
    call [scanf]        ; call scanf for number a
    add ESP, 4*2   
    
    push dword message2 ; push the parameter for message1 onto the stack
    call [printf]       ; call printf to print message1
    add ESP, 4*1        ; add 4 bytes to the stack to "jump" the parameters
        
    push dword b        ; push the parameter for a onto the stack
    push dword format   ; push the parameter for format onto the stack
    call [scanf]        ; call scanf for number a
    add ESP, 4*2 
    
    push dword message3 ; push the parameter for message1 onto the stack
    call [printf]       ; call printf to print message1
    add ESP, 4*1        ; add 4 bytes to the stack to "jump" the parameters
        
    push dword c        ; push the parameter for a onto the stack
    push dword format   ; push the parameter for format onto the stack
    call [scanf]        ; call scanf for number a
    add ESP, 4*2   
    
    mov ebx, [a]
    mov ecx, [b]
    mov edx, [c]
    
    push ebx
    push ecx
    push edx
    call calcul
    
    push eax
    push dword printformat
    call [printf]
    add esp, 4*2

    push 0
    call [exit]
