bits 32 
global start        
extern exit,printf ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll ; exit is a function that ends the calling process. It is defined in msvcrt.dll
import printf msvcrt.dll
; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions
; our data is declared here (the variables needed by our program)
segment data use32 class=data
	s1 db '1', '3', '5', '7' ; declararea sirului initial s1
	l equ ($-s1)*2 ; stabilirea lungimea sirului initial l
    s2 db '2', '4', '6', '8' ; declararea sirului initial s2
	d times l db 0,  0  ; rezervarea unui spatiu de dimensiune l pentru sirul destinatie d si initializarea acestuia
    format  db "Sirul este: %s", 0 ;definire format pentru afisare, daca vrem sa afisam sirul rezultat
segment code use32 class=code
start:
	mov ecx, l/2 ;punem lungimea in ECX pentru a putea realiza bucla loop de ecx ori
	mov esi, 0
    mov ebx, 0
	jecxz Sfarsit      
	Repeta:
		mov al, [s1+esi]     
		mov [d+ebx], al
        inc ebx
        mov al, [s2+esi]
        mov [d+ebx], al
        inc ebx
		inc esi
	loop Repeta
	Sfarsit:;terminarea programului
	
	push dword d ; punem parametrii pe stiva de la dreapta la stanga
	push dword format
	call [printf] ;apelam functia printf
	add esp, 4 * 2 ; eliberam parametrii de pe stiva
	; exit(0)
	push dword 0 ; push the parameter for exit onto the stack
	call [exit] ; call exit to terminate the program