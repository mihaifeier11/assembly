bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit, scanf, printf               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions
import scanf msvcrt.dll
import printf msvcrt.dll

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    number          dd 123456789
    format          db "%d", 0
    format_16       db "%x", 10, 13, 0
    format_print    db "%d ", 0
    multiplier      dd 0
    counter         db 0
    division_rest   dd 0

; our code starts here
segment code use32 class=code
    start:
        ;reading the number
        push dword [number]
        push dword format_16
        call [printf]
        add ESP, 4*2
        
        mov EAX, [number]
        mov EBX, 10
        
        ;finding the number of digits
        number_of_digits:
            mov EDX, 0
            div EBX
            inc byte [counter]
            cmp EAX, 0
            jne number_of_digits

        
        mov ECX, [counter]
        sub ECX, 1
        mov EAX, 1
        mov EBX, 10 
        
        multiplication:
            mul EBX
        loop multiplication
        
        mov [multiplier], EAX

        mov EBX, 10
        mov ECX, [counter]
        
        permutations:
            push ECX
            
            mov EAX, [number]
            mov EDX, 0
            
            div EBX
            
            mov [number], EAX
            mov [division_rest], EDX
            
            mov EAX, [division_rest]
            mov EDX, 0
            
            mul dword [multiplier]
            
            add [number], EAX
            
            push dword [number]
            push dword format_print
            call [printf]
            add ESP, 4*2
            
            pop ECX
        loop permutations
        

        
        
     
        
        
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
