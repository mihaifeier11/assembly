bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit, printf, scanf, fread, fprintf, fopen, fclose               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions
import fopen msvcrt.dll
import printf msvcrt.dll
import scanf msvcrt.dll
import fread msvcrt.dll
import fprintf msvcrt.dll
import fopen msvcrt.dll
import fclose msvcrt.dll

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    sir times 100 db 0
    acces_citire db "r", 0
    acces_scriere db "w", 0
    descriptor_citire dd -1
    descriptor_afisare dd -1
    nume_input db "input.txt", 0
    nume_output times 100 db 0
    len equ 100
    format_c db "%c", 0
    format db "%s", 0
    primul_cuvant times 20 db 0
    trei dd 3
    text times 100 db 0
    caractere_citite dd 0
    index dd 0
; our code starts here
segment code use32 class=code
    start:
        push dword acces_citire
        push dword nume_input
        call [fopen]
        add esp, 4*2
        
        mov [descriptor_citire], eax
        cmp eax, 0
        je final
        
        push dword [descriptor_citire]
        push dword len
        push dword 1
        push dword sir
        call [fread]
        add esp, 4*4
        mov [caractere_citite], eax
        
        bucla:  
            mov esi, [index]
            spatiu:
                cmp byte [sir+esi], " "
                
                je iesire
                inc esi
            loop spatiu
            iesire:
            
            mov ebx, esi
            mov esi, [index]
            mov edi, 0
            numeFisier:
                mov AL, [sir+esi]
                mov [nume_output+esi], AL
                mov [primul_cuvant+edi], AL
                inc esi
                inc edi
                cmp esi, ebx
                je iesireNumeFisier
            loop numeFisier
            iesireNumeFisier:
            mov byte [nume_output+esi], "."
            inc esi
            mov byte [nume_output+esi], "t" 
            inc esi
            mov byte [nume_output+esi], "x" 
            inc esi
            mov byte [nume_output+esi], "t" 
            
            
            push dword acces_scriere
            push dword nume_output
            call [fopen]
            add esp, 4*2
            mov [descriptor_afisare], eax
            
            mov esi, [index]
            propozitie:
                mov AL, [sir+esi]
                mov [text+esi], AL
                inc esi
                cmp AL, "."
                je afara
            loop propozitie
            mov [index], ESI
            afara:
            
            push dword text
            push dword [descriptor_afisare]
            call [fprintf]
            add esp, 4*2
            
            push dword [descriptor_afisare]
            call [fclose]
            add esp, 4
            
            mov EBX, [index]
            cmp EBX, [caractere_citite]
            jae iesireBucla
        dec EAX
        jnz bucla
        iesireBucla:
            
            
        
            
    
        final:
        push dword [descriptor_citire]
        call [fclose]
        add esp, 4
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
